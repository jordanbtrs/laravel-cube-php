<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Recipe extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @vararray<int, string>
     */
    protected $fillable = [
        /* fillable = remplissable */
        'name',
        'image',
        'steps',
        'preparation_duration',
        'cooking_duration',

    ];

    /**
     * The attributes that should be cast.
     *
     * @vararray<string, string>
     */
    protected $casts = [
        'steps' => 'array',
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];



}
