<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Recipe>
 */
class RecipeFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            //faker est une méthode qui permet de créer différent faux contenu pour la BDD
            'name' => $this->faker->name(),
            'image' => $this->faker->imageUrl(),
            'preparation_duration' => $this->faker->time('H:i'),
            'cooking_duration' => $this->faker->time('H:i'),
        ];
    }
}
