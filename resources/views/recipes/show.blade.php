@extends('layout.app')
@section('title', 'Recipe')

@section('content')


        <div class="row">
            <div class="col-md-12">
                <h1>{{ $recipe->name }}</h1>
                <p> Temps de préparation: {{ $recipe->preparation }}</p>
                <p>Durée de cuisson: {{ $recipe->cooking_duration }}</p>
                <img src="{{ asset( $recipe->image) }}" alt="{{ $recipe->name }}" class="img-fluid">

                @foreach ($recipe->steps as $step)
                    <b>{{ $step['key'] }}</b>: {{ $step['value'] }}<br />
                @endforeach

                <p>Créer le: {{ $recipe->created_at }}</p>
                <p>Mis à jour le: {{ $recipe->updated_at }}</p>
            </div>
        </div>

@endsection
