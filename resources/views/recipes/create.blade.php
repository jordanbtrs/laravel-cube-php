@extends('layout.app')
@section('title', 'Create Recipe')

@section('content')
    @if($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form action="{{ route('recipes.store') }}" method="post" enctype="multipart/form-data">
        @csrf
        <label for="name">Name</label>
        <input  type="text" name="name" id="name" value="">

        <!-- debut -->
    <div class="form-group mt-3">
            @for ($i=1; $i <= $numberSteps ; $i++)
                <div id="dynamicAddRemove">
                    <div class="col-md-2">
                        <label for="numb-step">Étape{{ ($i ) }}</label>
                        <input hidden id="num-step" type="text" name="steps[{{ $i }}][key]" class="form-control" value="étape {{ ($i + 1) }}">
                    </div>
                    <div class="col-md-4">
                        <input type="text" name="steps[{{ $i }}][value]" class="form-control" value="{{ old('steps['.$i.'][value]') }}">
                    </div>
                </div>
            @endfor
        <button type="button" name="add" id="dynamic-ar" class="btn btn-outline-primary" onclick="addStep();">Add Subject</button></td>
    </div>


        <!-- fin -->
    <div class="form-group mt-3">
        <label for="preparation_duration">Temps de préparation</label>
        <input  type="time" name="preparation_duration" id="preparation_duration" value="">
    </div>


        <div class="form-group mt-3">
            <label for="cooking_duration">Temps de cuisson</label>
            <input  type="time" name="cooking_duration" id="cooking_duration" value="">
        </div>

        <div class="form-group mt-3">
            <label for="image">Image</label>
            <input  type="file" name="image" id="image" value="">
        </div>


        <button type="submit" class="btn btn-primary mt-2" >Créer</button>
    </form>
@endsection


<!-- tentative de script JS pour ajouter dynamiquement des champs -->
<script type="text/javascript">


    let steps = document.getElementById('dynamicAddRemove');
    let addButton = document.getElementById('dynamic-ar');
    let deleteButton = document.getElementById('remove-input-field');

  /*  addButton.addEventListener('click', () => {
        steps.innerHTML += '<div class="row"><input type="text" name="step-{{ $i }}" placeholder="Enter subject" class="form-control" /><button type="button" id="remove-input-field" class="btn btn-outline-danger ">Delete</button></div>'
    }) */

    function addStep() {
        steps.innerHTML += '<div class="row"><input type="text" name="step-{{ $i }}" placeholder="Enter subject" class="form-control" /><button type="button" id="remove-input-field" class="btn btn-outline-danger ">Delete</button></div>'
    }

    deleteButton.addEventListener('click', () => {
        steps.innerHTML = steps.removeChild(steps.lastChild);
    })

</script>
