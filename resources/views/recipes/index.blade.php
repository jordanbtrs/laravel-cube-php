@extends('layout.app')
@section('title', 'Liste des recettes')

@section('content')

    <div class="row">
<!-- commentaire -->
    @foreach($recipes as $recipe)
            <div class="col-4">
                <div class="card mb-4 shadow-sm">
                    <div class="card-body">
                        <h5 class="card-title" style="text-transform: capitalize">{{$recipe->name}}</h5>
                        <img src="{{ asset($recipe->image) }}" alt="{{ $recipe->name }}" class="img-fluid">
                        <p class="card-text">préparation: {{ $recipe->preparation_duration }} </p>
                        <div class="container">
                            <div class="btn-toolbar justify-content-between">
                                <div class="btn-group">
                                    <a href="{{ route('recipes.show', ['recipe' => $recipe]) }}" class="btn btn-primary"><i class="bi bi-eye"></i></a>
                                    <a href="{{ route('recipes.edit', ['recipe' => $recipe]) }}" class="btn btn-primary"><i class="bi bi-pencil"></i></a>
                                </div>
                                <div class="btn-group">
                                    <form   action="{{ route('recipes.destroy', ['recipe' => $recipe]) }}" method="post">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-danger"><i class="bi bi-trash"></i> </button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    @endforeach

    </div>

@endsection





