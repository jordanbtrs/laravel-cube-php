@extends('layout.app')
@section('title', 'Modifier recette')

@section('content')
    <h1>{{$recipe->name}}</h1>
    <form action="{{route('recipes.update', $recipe->id)}}" method="post">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label for="name">Nom</label>
            <input type="text" class="form-control" id="name" name="name" value="{{$recipe->name}}">
        </div>


        @for ($i=1; $i <= count($recipe->steps); $i++)
            <div class="">
                <div class="col-md-2">
                    <label for="step{{$i}}">Étape {{$i}}</label>
                    <input id="step-{{$i}}" type="text" name="steps[{{ $i }}][key]" hidden class="form-control" value="{{ old('steps['.$i.'][key]') }}">
                </div>
                <div class="col-md-4">
                    <input type="text" name="steps[{{ $i }}][value]" class="form-control" value="{{ old('steps['.$i.'][value]') }}">
                </div>
            </div>
        @endfor


        <div class="form-group">
            <label for="image">Image</label>
            <input type="file" class="form-control" id="image" name="image" value="">
        </div>

        <div class="form-group">
            <label for="preparation_time">Temps de préparation</label>
            <input type="time" class="form-control" id="preparation_time" name="preparation_time" value="{{$recipe->preparation_time}}">
        </div>
        <div class="form-group">
            <label for="cooking_time">Temps de cuisson</label>
            <input type="time" class="form-control" id="cooking_time" name="cooking_time" value="{{$recipe->cooking_time}}">
        </div>

        <button type="submit" class="btn btn-primary mt-2">Mettre à jour</button>

@endsection
