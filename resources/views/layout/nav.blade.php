
<nav class="navbar navbar-expand-lg navbar-light bg-light mb-5">

    <div class="container-fluid">
        <a class="navbar-brand" href="#">Larecette</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav me-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="{{ route('recipes.index') }}">Liste des recettes <span class="sr-only"></span></a>
                </li>

                <li class="nav-item active">
                    <a class="nav-link" href="{{ route('recipes.create') }}">Créer une recette <span class="sr-only"></span></a>
                </li>
                <!-- @if(Auth::check())
                    <li class="nav-item active">
                        <p></p>
                    </li>
@endif -->
            </ul>
            <ul class="navbar-nav me-2">
                @guest
                    <li class="nav-item active">
                        <a class="nav-link" href="#">Connexion <span class="sr-only"></span></a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="#">Inscription <span class="sr-only"></span></a>
                    </li>
                @endguest
                @auth()
                    <li class="nav-item active">
                        <a class="nav-link" href="#">Déconnexion <span class="sr-only"></span></a>
                    </li>
                @endauth
            </ul>
        </div>
    </div>
</nav>
