<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title')</title>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css" />
</head>

<body>

    @include('layout.nav')
    @include('layout.notify')

    @yield('content')

<script src="{{ asset('js/app.js') }}" type="application/javascript"></script>
</body>
</html>
